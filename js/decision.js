var Abacus = {

  pick: function(choice) {
    var choices = ['control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'control', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceC', 'control', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'control', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceC', 'control', 'broadcastPriceB', 'control', 'control', 'control', 'control', 'control', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'control', 'control', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceC', 'control', 'broadcastPriceC', 'control', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceB', 'control', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'control', 'broadcastPriceB', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceC', 'broadcastPriceB', 'broadcastPriceB'];
    if (choice) choice = choice - 1; // passed in a group
    else        choice = Math.floor(Math.random()*choices.length); // 0 to 99
    
    return { id: (choice+1), name: choices[choice]}
  }

}